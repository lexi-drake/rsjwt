# RSJwt

[RSJwt](https://www.nuget.org/packages/RSJwt/) is a **R**eally **S**mall **Jwt** library for dotnet. This is intended to work with the default Microsoft IoC container.

## Install

```bash
dotnet add package RSJwt
```

## Usage

### Initialize the Jwt authentication config

```c#
// Startup.cs
using LD.RSJwt;

// ...

public void ConfigureServices(IServiceCollection services)
{
    // ...
    var secret = "your secret"; 
    var issuer = "your issuer";
    var audience = "your audience";
    services.AddRSJwt(JwtConfig.BaseConfig(secret, issuer, audience));
}
```

### Use anywhere (through DI)

```c#
using LD.RSJwt;

// ...

public class UserController : BaseController
{
    private readonly IJwtHandler _handler;

    public UserController(IJwtHandler handler)
    {
        _handler = handler;
    }
}
```

### Generate a Jwt

```c#
using LD.RSJwt;

// ...

var userObject = new UserObject()
{
    Name = "username",
    Role = "Premium"
};

var jwt = _handler.ConvertToJwt(userObject);_
```

### Parse a Jwt

```c#
using LD.RSJwt;

// ...

var user = _handler.ExtractFromJwt<UserObject>(jwt);
```

### Authenticate

```c#

using LD.RSJwt;

// ...

public class MyController : BaseController
{
    private readonly IJwtHandler _handler;

    public MyController(IJwtHandler handler)
    {
        _handler = handler;
    }

    [HttpGet]
    // Restrict access to only one role
    [RsJwtAuthorization("Role", "Admin")]
    public async Task<ActionResult> GetAdminOnlyInfo()
    {
        //...
        return OkResult();
    }


    [HttpGet]
    // Restrict access to N roles
    [RsJwtAuthorization("Role", new []{"Agent", "Admin"})]
    public async Task<ActionResult> GetInfo()
    {
        // ...
        return OkResult();
    }
}

```