using System;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace LD.RSJwt
{
    public class RSJwtAuthorizationFilter : IAuthorizationFilter
    {
        private readonly string _key;
        private readonly string[] _values;

        public RSJwtAuthorizationFilter(string key, string value)
        {
            _key = key;
            _values = new string[] { value };
        }

        public RSJwtAuthorizationFilter(string key, string[] values)
        {
            _key = key;
            _values = values;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            var isAuthorized = context.HttpContext.User.Claims.Any(c => c.Type == _key && _values.Contains(c.Value));
            if (!isAuthorized)
            {
                context.Result = new ForbidResult();
            }
        }
    }
}