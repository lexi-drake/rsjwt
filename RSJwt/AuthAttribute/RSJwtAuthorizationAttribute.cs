using Microsoft.AspNetCore.Mvc;

namespace LD.RSJwt
{
    public class RSJwtAuthorizationAttribute : TypeFilterAttribute
    {
        public RSJwtAuthorizationAttribute(string key, string value)
            : base(typeof(RSJwtAuthorizationFilter))
        {
            Arguments = new object[] { key, value };
        }

        public RSJwtAuthorizationAttribute(string key, string[] values)
            : base(typeof(RSJwtAuthorizationFilter))
        {
            Arguments = new object[] { key, values };
        }
    }


}