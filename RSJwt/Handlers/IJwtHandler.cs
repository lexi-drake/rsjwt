namespace LD.RSJwt
{
    public interface IJwtHandler
    {
        T ExtractFromJwt<T>(string jwt) where T : new();
        string ConvertToJwt<T>(T obj);
    }
}