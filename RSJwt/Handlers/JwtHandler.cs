using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace LD.RSJwt
{
    public class JwtHandler : IJwtHandler
    {
        private JwtConfig _config;

        public JwtHandler(JwtConfig config)
        {
            if (config == null)
            {
                throw new ArgumentNullException("JwtConfig");
            }
            _config = config;
        }

        public T ExtractFromJwt<T>(string jwt) where T : new()
        {
            if (string.IsNullOrEmpty(jwt))
            {
                throw new ArgumentNullException("jwt");
            }

            var parameters = _config.GetValidationParameters();
            parameters.ValidateLifetime = false;
            var principal = new JwtSecurityTokenHandler().ValidateToken(jwt, parameters, out var securityToken);
            var claimsDict = new Dictionary<string, string>();
            foreach (var claim in principal.Claims)
            {
                claimsDict.Add(claim.Type, claim.Value);
            }

            var t = new T();
            foreach (var property in typeof(T).GetProperties())
            {
                var valid = claimsDict.TryGetValue(property.Name, out var value);
                if (!valid)
                {
                    throw new ArgumentNullException($"property: {typeof(T)}");
                }
                var propType = property.PropertyType;
                var converter = TypeDescriptor.GetConverter(propType);
                property.SetValue(t, converter.ConvertFromString(value));
            }
            return t;
        }

        public string ConvertToJwt<T>(T obj)
        {
            var claims = from property in obj.GetType().GetProperties()
                         select new Claim(property.Name, property.GetValue(obj).ToString());
            var tokenDescriptor = _config.GetDescriptor(new ClaimsIdentity(claims));
            var tokenHandler = new JwtSecurityTokenHandler();
            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}