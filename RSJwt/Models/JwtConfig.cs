using System;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace LD.RSJwt
{
    public class JwtConfig
    {
        private readonly byte[] _secret;
        private readonly string _issuer;
        private readonly string _audience;
        private bool _validateLifetime;
        private int _expirationMinutes;
        private string _algorithm;

        private JwtConfig(byte[] secret, string issuer, string audience)
        {
            _secret = secret;
            _issuer = issuer;
            _audience = audience;
            _validateLifetime = true;
            _expirationMinutes = 60;
            _algorithm = SecurityAlgorithms.HmacSha256Signature;
        }

        public static JwtConfig BaseConfig(string secret, string issuer, string audience) =>
            new JwtConfig(Encoding.UTF8.GetBytes(secret), issuer, audience);

        public JwtConfig ThatWillNotValidateLifetime()
        {
            this._validateLifetime = false;
            return this;
        }

        public JwtConfig WithExpirationMinutes(int minutes)
        {
            this._expirationMinutes = minutes;
            return this;
        }

        public JwtConfig WithAlgorithm(string algorithm)
        {
            this._algorithm = algorithm;
            return this;
        }

        public SecurityTokenDescriptor GetDescriptor(ClaimsIdentity subject) =>
            new SecurityTokenDescriptor()
            {
                Subject = subject,
                Expires = DateTime.Now.AddMinutes(_expirationMinutes),
                NotBefore = DateTime.Now,
                IssuedAt = DateTime.Now,
                Issuer = _issuer,
                Audience = _audience,
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(_secret), _algorithm)
            };

        public TokenValidationParameters GetValidationParameters() =>
            new TokenValidationParameters()
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(_secret),
                ValidateIssuer = !String.IsNullOrEmpty(_issuer),
                ValidIssuer = _issuer,
                ValidateAudience = !String.IsNullOrEmpty(_audience),
                ValidAudience = _audience,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };
    }
}