using System.Linq;

namespace LD.RSJwt
{
    public static class StringExtensions
    {
        internal static string ExtractJwtFromCookie(this string cookie) =>
            string.IsNullOrEmpty(cookie) ? "" : cookie.Split(';').First().Trim();
    }
}