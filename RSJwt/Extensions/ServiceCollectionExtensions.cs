using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace LD.RSJwt
{
    public static class ServiceCollectionExtensions
    {
        public static void AddRSJwt(this IServiceCollection services, JwtConfig config)
        {
            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(options =>
            {
                options.Events = new JwtBearerEvents()
                {
                    OnMessageReceived = context =>
                    {
                        var token = context.Request.Cookies["jwt"].ExtractJwtFromCookie();
                        context.Token = token;
                        return Task.CompletedTask;
                    }
                };
                options.TokenValidationParameters = config.GetValidationParameters();
            });

            services.AddScoped<IJwtHandler>(s => new JwtHandler(config));
        }
    }
}