using System;
using System.Collections.Generic;
using System.Security.Claims;
using Microsoft.AspNetCore.Mvc.Abstractions;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.AspNetCore.Routing;
using Xunit;
using Moq;
using LD.RSJwt;
using System.Net.Http;
using Microsoft.AspNetCore.Http.Features;
using System.Threading;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;

namespace Tests
{
    public class RSJwtAuthorizationFilterShould
    {
        private string _key;
        private string _value;
        private AuthorizationFilterContext _context;

        public RSJwtAuthorizationFilterShould()
        {
            _key = Guid.NewGuid().ToString();
            _value = Guid.NewGuid().ToString();

            var httpContext = new Mock<HttpContext>();
            httpContext.Setup(x => x.User)
                .Returns(new ClaimsPrincipal(new List<ClaimsIdentity>() { new ClaimsIdentity(new Claim[] { new Claim(_key, _value) }) }));

            var actionContext = new ActionContext(httpContext.Object, new RouteData(), new ActionDescriptor());
            _context = new AuthorizationFilterContext(actionContext, new List<IFilterMetadata>());
        }

        [Fact]
        public void ShouldRejectIfInvalidClaimKey()
        {
            var filter = new RSJwtAuthorizationFilter(Guid.NewGuid().ToString(), _value);
            filter.OnAuthorization(_context);

            Assert.Equal(typeof(ForbidResult), _context.Result.GetType());
        }

        [Fact]
        public void ShouldRejectIfInvalidClaimValue()
        {
            var filter = new RSJwtAuthorizationFilter(_key, Guid.NewGuid().ToString());
            filter.OnAuthorization(_context);

            Assert.Equal(typeof(ForbidResult), _context.Result.GetType());
        }

        [Fact]
        public void ShouldRejectIfFuncNotSatisfied()
        {
            var filter = new RSJwtAuthorizationFilter(_key, new string[1]);
            filter.OnAuthorization(_context);

            Assert.Equal(typeof(ForbidResult), _context.Result.GetType());
        }

        [Fact]
        public void ShouldAcceptIfValidClaimUsingKeyValuePair()
        {
            var filter = new RSJwtAuthorizationFilter(_key, _value);
            filter.OnAuthorization(_context);

            Assert.Null(_context.Result);
        }

        [Fact]
        public void ShouldAcceptIfValidClaimsUsingArray()
        {
            var value = _value.Substring(0, 5);
            var filter = new RSJwtAuthorizationFilter(_key, new string[] { value });

            Assert.Null(_context.Result);
        }
    }
}
