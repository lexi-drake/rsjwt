using System;
using Xunit;
using LD.RSJwt;

namespace Tests
{
    public class JwtHandlerShould
    {
        [Fact]
        public void ThrowIfNullConfig()
        {
            var ex = Assert.Throws<ArgumentNullException>(() =>
           {
               var handler = new JwtHandler(null);
           });
            Assert.Equal("Value cannot be null. (Parameter 'JwtConfig')", ex.Message);
        }

        [Fact]
        public void ConvertSuccessfully()
        {
            var handler = new JwtHandler(GetConfig());
            var obj = GetTestObj();

            var jwt = handler.ConvertToJwt(obj);
            Assert.NotNull(jwt);
        }

        [Fact]
        public void ExtractSuccessfully()
        {
            var handler = new JwtHandler(GetConfig());
            var originalObj = GetTestObj();

            var jwt = handler.ConvertToJwt(originalObj);
            var newObj = handler.ExtractFromJwt<TestObj>(jwt);

            Assert.Equal(originalObj.Id, newObj.Id);
            Assert.Equal(originalObj.Name, newObj.Name);
            Assert.Equal(originalObj.Role, newObj.Role);
            Assert.Equal(originalObj.Age, newObj.Age);
        }

        private JwtConfig GetConfig() =>
            JwtConfig.BaseConfig(Guid.NewGuid().ToString(), Guid.NewGuid().ToString(), Guid.NewGuid().ToString());

        private TestObj GetTestObj() =>
                    new TestObj()
                    {
                        Id = Guid.NewGuid().ToString(),
                        Name = Guid.NewGuid().ToString(),
                        Role = Guid.NewGuid().ToString(),
                        Age = 10
                    };

        private class TestObj
        {
            public string Id { get; set; }
            public string Name { get; set; }
            public string Role { get; set; }
            public int Age { get; set; }
        }
    }
}
